const config = {
  secret: 'jsonwebtoken',
  db_url: 'mongodb://localhost:27017/pnd',
  local_url: 'mongodb://localhost:27017/pnd'
};
module.exports = config;