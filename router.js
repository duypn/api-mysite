var express = require("express");
var router = express.Router();
var routerAuthen = express.Router();

var info = require('./app/services/Info');
var skill = require('./app/services/Skill');
var resume = require('./app/services/Resume');
var project = require('./app/services/Project');
var social = require('./app/services/Social');

var jwt    = require('jsonwebtoken');


//--------authentic----------//
// routerAuthen.use(function(req, res, next) {
//   console.log("req "+ req._parsedUrl.pathname);
//   console.log("res "+ res);
//   console.log("res header "+ req.headers['x-access-token']);
//   var token = req.body.token || req.query.token || req.headers['x-access-token'];
//   if(req._parsedUrl.pathname=="/logincustomer" || req._parsedUrl.pathname=="/logincustomer/" || req._parsedUrl.pathname=="/customer"){
//     next();
//   }
//   else{
//     if (token) {
//       jwt.verify(token, 'sspakeySecret', function(err, decoded) {      
//         if (err) {
//           return res.json({ success: false, message: 'Failed to authenticate token.' });    
//         } else {
//           req.decoded = decoded;    
//           next();
//         }
//       });
//     } else {
//       return res.status(403).send({ 
//         success: false, 
//         message: 'No token provided.' 
//       });
//     }
//   }
// });

//info router
router.route('/infos').get(info.get);
router.route('/info').post(info.create);
router.route('/info/:id').patch(info.update);

//skill router
router.route('/skills').get(skill.get);
router.route('/skill').post(skill.create);
router.route('/skill/:id').get(skill.getById);
router.route('/skill/:id').patch(skill.update);

//resume router
router.route('/resumes').get(resume.get);
router.route('/resume').post(resume.create);
router.route('/resume/:id').get(resume.getById);
router.route('/resume/:id').patch(resume.update);

//resume router
router.route('/projects').get(project.get);
router.route('/project').post(project.create);
router.route('/project/:id').get(project.getById);
router.route('/project/:id').patch(project.update);

//resume router
router.route('/socials').get(social.get);
router.route('/social').post(social.create);
router.route('/social/:id').get(social.getById);
router.route('/social/:id').patch(social.update);

module.exports = router;