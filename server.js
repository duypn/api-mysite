var express    = require('express');
var cors 	   = require('cors');
var app        = express();
var router     = require("./router");
var bodyParser = require('body-parser');
var mongoose   = require("mongoose");
var config     = require('./server-config');

var whitelist = ['http://localhost:4200', 'http://localhost:5001']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response 
  }else{
    corsOptions = { origin: false } // disable CORS for this request 
  }
  callback(null, corsOptions) // callback expects two parameters: error and options 
}

mongoose.connect(config.local_url, {
    useMongoClient: true
}, function () {
    console.log('connected to mongodb');
	
	app.use(cors(corsOptionsDelegate));
	
    app.use(function(rep,res,next){
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');
        
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();

    });


// configure app to use bodyParser()
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

// REGISTER OUR ROUTES -------------------------------

// all of our routes will be prefixed with /api
    app.use('/api', router);

    var port = process.env.PORT || 5000; // set our port

// START THE SERVER
// =============================================================================
    app.listen(port);
});