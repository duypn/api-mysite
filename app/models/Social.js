var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Social = new Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    icon: String,
    created_at: Date,
    update_at: Date
});

module.exports = mongoose.model('Social', Social);