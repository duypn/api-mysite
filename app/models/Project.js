var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Project = new Schema({
    title: {
        type: String,
        required: true
    },
    image:String,
    url:{
        type: String,
        required: true
    },
    type:String,
    technology: String,
    description: String,
    created_at: Date,
    update_at: Date
});

module.exports = mongoose.model('Project', Project);