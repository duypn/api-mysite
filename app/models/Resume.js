var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Resume = new Schema({
    title: {
        type: String,
        required: true
    },
    point: {
        type: Number,
        required: true,
        max: 10
    },
    school: {
        type: String,
        required: true
    },
    description: String,
    start_time: {
        type: String,
        required: true
    },
    end_time: {
        type: String,
        required: true
    },
    created_at: Date,
    update_at: Date
});

module.exports = mongoose.model('Resume', Resume);