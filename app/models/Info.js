var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Info = new Schema({
    name: {
        type: String,
        required: true
    },
    avatar: String,
    phone: {
        type: String,
        required: true
    },
    email: String,
    address: {
        type: String,
        required: true
    },
    location: String,
    chanel: String,
    title: {
        type: String,
        required: true
    },
    description: String,
    created_at: Date,
    update_at: Date
});

module.exports = mongoose.model('Info', Info);