var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Skill = new Schema({
    title: {
        type: String,
        required: true
    },
    percent: {
        type: Number,
        required: true,
        max: 100
    },
    description: String,
    created_at: Date,
    update_at: Date
});

module.exports = mongoose.model('Skill', Skill);