let Project = require('../models/Project')

module.exports = {
    get: async function (req, res) {
        Project.find((err, proj) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(proj);
        });
    },
    create: async function (req, res) {
        let model = new Project();

        model.title = req.body.title;
        model.image = req.body.image;
        model.url = req.body.url;
        model.type = req.body.type,
        model.technology =  req.body.technology;
        model.description = req.body.description;
        model.created_at = Date.now();
        model.update_at = Date.now();

        //post data
        model.save((err, proj) => {
            if (err)
                res.status(500).send(err);
            
            res.status(200).send(proj);
        })
    },
    getById: async function(req,res){
        let id = req.params.id;

        Project.findOne({_id:id},(err, proj) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(proj);
        });
    },
    update: async function(req,res) {
        let id = req.params.id;

        let model = new Project();
        model = model.toObject();

        model.title = req.body.title;
        model.image = req.body.image;
        model.url = req.body.url;
        model.type = req.body.type,
        model.technology =  req.body.technology;
        model.description = req.body.description;
        model.update_at = Date.now();

        delete model["_id"];

        Project.findByIdAndUpdate(id,model, function(err){
            if(err)
                res.status(500).send(err)
            
            res.status(200).send({ status: 200, message: 'ok'});
        })
    
    }
}