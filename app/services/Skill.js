let Skill = require('../models/Skill')

module.exports = {
    get: async function (req, res) {
        Skill.find((err, skills) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(skills);
        });
    },
    create: async function (req, res) {
        let model = new Skill();

        model.title = req.body.title;
        model.percent = req.body.percent;
        model.description = req.body.description;
        model.created_at = Date.now();
        model.update_at = Date.now();

        //post data
        model.save((err, skill) => {
            if (err)
                res.status(500).send(err);
            
            res.status(200).send(skill);
        })
    },
    getById: async function(req,res){
        let id = req.params.id;

        Skill.findOne({_id:id},(err, skill) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(skill);
        });
    },
    update: async function(req,res) {
        let id = req.params.id;

        let model = new Skill();
        model = model.toObject();

        model.title = req.body.title;
        model.percent = req.body.percent;
        model.description = req.body.description;
        model.update_at = Date.now();

        delete model["_id"];

        Skill.findByIdAndUpdate(id,model, function(err){
            if(err)
                res.status(500).send(err)
            
            res.status(200).send({ status: 200, message: 'ok'});
        })
    
    }
}