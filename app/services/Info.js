let Info = require('../models/Info')

module.exports = {
    get: async function (req, res) {
        Info.findOne((err, info) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(info);
        });
    },
    create: async function (req, res) {
        let model = new Info();

        model.name = req.body.name;
        model.avatar = req.body.avatar;
        model.phone = req.body.phone;
        model.email = req.body.email;
        model.address = req.body.address;
        model.location = req.body.location;
        model.chanel = req.body.chanel;
        model.title = req.body.title;
        model.created_at = Date.now();
        model.update_at = Date.now();

        //post data
        model.save((err, info) => {
            if (err)
                res.status(500).send(err);
            
            res.status(200).send(info);
        })
    },
    update: async function(req,res) {
        let id = req.params.id;

        let model = new Info();
        model = model.toObject();

        model.name = req.body.name;
        model.avatar = req.body.avatar;
        model.phone = req.body.phone;
        model.email = req.body.email;
        model.address = req.body.address;
        model.location = req.body.location;
        model.chanel = req.body.chanel;
        model.title = req.body.title;
        model.update_at = Date.now();

        delete model["_id"];

        Info.findByIdAndUpdate(id,model, function(err){
            if(err)
                res.status(500).send(err)
            
            res.status(200).send({ status: 200, message: 'ok'});
        })
    
    }
}