let Resume = require('../models/Resume')

module.exports = {
    get: async function (req, res) {
        Resume.find((err, edu) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(edu);
        });
    },
    create: async function (req, res) {
        let model = new Resume();

        model.title = req.body.title;
        model.point = req.body.point;
        model.description = req.body.description;
        model.school = req.body.school,
        model.start_time =  req.body.start_time;
        model.end_time = req.body.end_time;
        model.created_at = Date.now();
        model.update_at = Date.now();

        //post data
        model.save((err, edu) => {
            if (err)
                res.status(500).send(err);
            
            res.status(200).send(edu);
        })
    },
    getById: async function(req,res){
        let id = req.params.id;

        Resume.findOne({_id:id},(err, edu) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(edu);
        });
    },
    update: async function(req,res) {
        let id = req.params.id;

        let model = new Resume();
        model = model.toObject();

        model.title = req.body.title;
        model.point = req.body.point;
        model.description = req.body.description;
        model.school = req.body.school,
        model.start_time =  req.body.start_time;
        model.end_time = req.body.end_time;
        model.update_at = Date.now();

        delete model["_id"];

        Resume.findByIdAndUpdate(id,model, function(err){
            if(err)
                res.status(500).send(err)
            
            res.status(200).send({ status: 200, message: 'ok'});
        })
    
    }
}