let Social = require('../models/Social')

module.exports = {
    get: async function (req, res) {
        Social.find((err, soc) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(soc);
        });
    },
    create: async function (req, res) {
        let model = new Social();

        model.name = req.body.name;
        model.url = req.body.url;
        model.icon = req.body.icon;
        model.created_at = Date.now();
        model.update_at = Date.now();

        //post data
        model.save((err, soc) => {
            if (err)
                res.status(500).send(err);
            
            res.status(200).send(soc);
        })
    },
    getById: async function(req,res){
        let id = req.params.id;

        Social.findOne({_id:id},(err, soc) => {
            if (err)
                res.status(500).send(err);

            res.status(200).json(soc);
        });
    },
    update: async function(req,res) {
        let id = req.params.id;

        let model = new Social();
        model = model.toObject();

        model.name = req.body.name;
        model.url = req.body.url;
        model.icon = req.body.icon;
        model.update_at = Date.now();

        delete model["_id"];

        Social.findByIdAndUpdate(id,model, function(err){
            if(err)
                res.status(500).send(err)
            
            res.status(200).send({ status: 200, message: 'ok'});
        })
    
    }
}